package com.example.simplespringexercises.exercise2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component

public class CommandLineRunnerWithFieldInjection implements CommandLineRunner {

    @Autowired
    private DummyLogger1 dummyLogger1;

    @Override
    public void run(final String... args) throws Exception {
        dummyLogger1.sayHello();
    }
}
