package com.example.simplespringexercises.exercise2;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor

public class CommandLineRunnerWithConstructorInjection implements CommandLineRunner{
    private final DummyLogger1 dummyLogger1;

    @Override
    public void run(final String... args) throws Exception {
        dummyLogger1.sayHello();
    }
}
