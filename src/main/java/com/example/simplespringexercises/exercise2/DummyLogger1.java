package com.example.simplespringexercises.exercise2;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class DummyLogger1 {
    public void sayHello() {
        log.info("hello from DummyLogger");
    }
}
