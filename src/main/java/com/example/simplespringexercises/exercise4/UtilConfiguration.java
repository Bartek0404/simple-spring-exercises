package com.example.simplespringexercises.exercise4;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UtilConfiguration {
    @Bean
    public DummyLogger4 dummyLogger4(){
        return new DummyLogger4();
    }
    @Bean
    public ListUtil listUtility(){
        return new ListUtil();
    }
    @Bean
    public StringUtil stringUtility(){
        return new StringUtil();
    }
}
