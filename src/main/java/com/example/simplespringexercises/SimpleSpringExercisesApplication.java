package com.example.simplespringexercises;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleSpringExercisesApplication {

    public static void main(String[] args) {
        SpringApplication.run(SimpleSpringExercisesApplication.class, args);
    }

}
