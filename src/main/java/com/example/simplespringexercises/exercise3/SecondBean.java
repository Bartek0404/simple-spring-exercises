package com.example.simplespringexercises.exercise3;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class SecondBean implements DummyLoggerIFace{
    @Override
    public void sayHello() {
        log.info("Hello from second Bean");
    }
}
