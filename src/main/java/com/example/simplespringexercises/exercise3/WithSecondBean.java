package com.example.simplespringexercises.exercise3;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class WithSecondBean implements CommandLineRunner {
        private DummyLoggerIFace dummyLoggerIFace;
    public WithSecondBean(@Qualifier("secondBean") final DummyLoggerIFace dummyLoggerIFace) {
        this.dummyLoggerIFace = dummyLoggerIFace;
    }
    @Override
    public void run(String... args) throws Exception {
        dummyLoggerIFace.sayHello();
    }
}
