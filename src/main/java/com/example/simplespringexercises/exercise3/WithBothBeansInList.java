package com.example.simplespringexercises.exercise3;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class WithBothBeansInList implements CommandLineRunner {
    private final List<DummyLoggerIFace> beans;

    public WithBothBeansInList(final List<DummyLoggerIFace> beans) {
        this.beans = beans;
    }
    @Override
    public void run(String... args) throws Exception {
        beans.forEach(DummyLoggerIFace::sayHello);
    }
}
