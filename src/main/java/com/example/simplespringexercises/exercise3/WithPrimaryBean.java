package com.example.simplespringexercises.exercise3;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor

public class WithPrimaryBean implements CommandLineRunner {

    private final DummyLoggerIFace dummyLoggerIFace;
    @Override
    public void run(String... args) throws Exception {
        dummyLoggerIFace.sayHello();
    }
}
