package com.example.simplespringexercises.exercise3;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@Primary
public class FirstBean implements DummyLoggerIFace{
    @Override
    public void sayHello() {
        log.info("Hello from primary Bean");
    }
}
